package com.example.downloadandsavetostorage.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.example.downloadandsavetostorage.domain.UtilityHelper
import com.example.downloadandsavetostorage.presentation.viewmodel.DataViewModel
import com.example.downloadandsavetostorage.ui.theme.DownloadAndSaveToStorageTheme
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DownloadAndSaveToStorageTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val context = LocalContext.current
                    DownloadData(DataViewModel(UtilityHelper(context)))
                }
            }

        }
    }
}

@Composable
fun DownloadData (viewmodel: DataViewModel = viewModel()) {
    val theDataState by remember { viewmodel.theDataState}
    viewmodel.getdata()
    viewmodel.getDataFromFile()

    //Display the data
    Column(modifier = Modifier.fillMaxWidth()) {
        //Convert the long string to pretty JSON
        val gson = GsonBuilder().setPrettyPrinting().create()
        val prettyJson = gson.toJson(JsonParser.parseString(theDataState))
        Text(prettyJson)

        //Text(text = "$theDataState")
    }
}
